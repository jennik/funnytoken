# README #

Конечно лучше было бы использовать jwt, или что-то типа-того, тогда токены в бд не нужно было бы хранить, можно аутентифицировать пользователей хоть прямо в nginx'e.
Но в задании есть четкое указание на хранение токенов в БД: 
> Выпуск токена подразумевает сохранение его в БД.

Короче, сначала хотел реализовать все используя Symfony Security компонент. Но он довольно сложный для понимания. Поэтому, считая что времени у меня немного, решил написать свой велосипед (**UPD: в итоге сделал на Symfony Guard Authentication, см. ветку symfony-auth**):  
- все контроллеры в файле src/controllers.php  
- настройка сервисов в src/app.php  
- пароль передается в открытом виде (нужно использовать https)  
- аутентификация юзера происходит прямо в контроллере (да, эт не оч круто)  
- пользователи и токены хранятся в db.sqlite3 (включена в индекс git'a)  

### Как работает API: ###
Весь обмен данными производится в json (для запросов с данными в теле нужно включать заголовок Content-type: application/json).  

Для создания нового токена используется запрос
```
POST /token/new
Content-Type: application/json

{
	"user": "vasya",
	"password": "foo"
}
```
Результат:
```
201 {"token":"7be057e24db15a1e7d553ca814fe02690315883bb2bcfe7d18cdb834b42eb917"}
```

Для получения имени по пользователю:
```
GET /user/name?token=a85c92c3b5a593665e5fb19fbe4b4affe741e4a72669eb19ec523403ca4645f9
```
Результат:
```
200 {"name":"vasya"}
```
В БД 2 тестовых юзера:

юзер  | пароль  
vasya | foo  
petya | bar  

### Как запустить на своем компе? ###
```
cd path/to/root/of/the/app
mkdir conf && touch conf/conf.php
composer install
php -S localhost:8080 -t web web/index.php
```