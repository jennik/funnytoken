<?php
namespace FunnyToken;

interface AuthenticationProviderInterface
{
    public function isAuthenticated(UserInterface $user);
}