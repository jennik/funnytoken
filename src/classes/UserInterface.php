<?php
namespace FunnyToken;

interface UserInterface
{

    public function setName($name);

    public function getName();

    public function setPasswordHash($passwordHash);

    public function getPasswordHash();

    public function setId($id);

    public function getId();
}