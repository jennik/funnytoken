<?php
namespace FunnyToken\Token;

use FunnyToken\UserInterface;

interface TokenGeneratorInterface
{
    public function newToken(UserInterface $user);
}