<?php
namespace FunnyToken\Token;

use FunnyToken\UserInterface;

interface StorageInterface
{
    public function add($token, UserInterface $user);
}