<?php
namespace FunnyToken\Token;

use FunnyToken\UserInterface;

class DBStorage implements StorageInterface
{
    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function add($token, UserInterface $user)
    {
        $id = $user->getId();
        if (!$id) {
            throw new \UnexpectedValueException("user doesn't have an id");
        }

        $this->db->insert('tokens', ['token' => $token, 'userId' => $id]);
    }
}