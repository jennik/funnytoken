<?php
namespace FunnyToken\Token;

use FunnyToken\UserInterface;

class TokenGenerator implements TokenGeneratorInterface
{
    protected $ts;

    public function __construct(StorageInterface $ts)
    {
        $this->ts = $ts;
    }

    public function newToken(UserInterface $user, $salt = null, $saltLength = 10)
    {
        if ($salt === null) {
            $salt = random_bytes($saltLength);
        }

        $token = hash('sha256', microtime() . $user->getName() . $salt);

        if ($this->ts) {
            $this->ts->add($token, $user);
        }

        return $token;
    }
}