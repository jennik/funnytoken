<?php
namespace FunnyToken\Token;

interface UserProviderInterface
{
    public function getUserByToken($token);
}