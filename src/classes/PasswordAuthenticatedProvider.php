<?php
namespace FunnyToken;

class PasswordAuthenticatedProvider implements AuthenticationProviderInterface
{
    protected $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function isAuthenticated(UserInterface $user, $password = null)
    {
        return $password !== null && $this->passwordEncoder->isPasswordValid($user, $password);
    }
}