<?php
namespace FunnyToken;

class DBUserProvider implements UserProviderInterface, Token\UserProviderInterface
{
    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getUserByName($name)
    {
        $userData = $this->db->fetchAssoc('SELECT id, name, password FROM users WHERE name = :name', ['name' => $name]);
        if ($userData) {
            $user = new User($userData['name']);
            $user->setId($userData['id']);
            $user->setPasswordHash($userData['password']);
            return $user;
        }

        return null;
    }

    public function getUserById($id)
    {
        $userData = $this->db->fetchAssoc('SELECT id, name, password FROM users WHERE id = :id', ['id' => $id]);
        if ($userData) {
            $user = new User($userData['name']);
            $user->setId($userData['id']);
            $user->setPasswordHash($userData['password']);
            return $user;
        }

        return null;
    }

    public function getUserByToken($token)
    {
        $userData = $this->db->fetchAssoc('SELECT id, name, password FROM users u JOIN tokens t ON u.id = t.userId WHERE token = :token', ['token' => $token]);
        if ($userData) {
            $user = new User($userData['name']);
            $user->setId($userData['id']);
            $user->setPasswordHash($userData['password']);
            return $user;
        }
        return null;
    }
}