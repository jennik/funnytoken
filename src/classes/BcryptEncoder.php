<?php
namespace FunnyToken;

class BcryptEncoder implements UserPasswordEncoderInterface
{
    public function encodePassword(UserInterface $user, $plainPassword)
    {
        return password_hash($plainPassword, PASSWORD_BCRYPT);
    }

    public function isPasswordValid(UserInterface $user, $raw)
    {
        $hash = $user->getPasswordHash();

        return password_verify($raw, $hash);
    }
}