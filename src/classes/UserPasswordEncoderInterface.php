<?php
namespace FunnyToken;

interface UserPasswordEncoderInterface
{
    public function encodePassword(UserInterface $user, $plainPassword);

    public function isPasswordValid(UserInterface $user, $raw);
}