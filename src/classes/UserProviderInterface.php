<?php
namespace FunnyToken;

interface UserProviderInterface
{
    public function getUserByName($name);

    public function getUserById($id);
}