<?php
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use FunnyToken\DBUserProvider;
use FunnyToken\BcryptEncoder;
use FunnyToken\PasswordAuthenticatedProvider;
use FunnyToken\Token\TokenGenerator;
use FunnyToken\Token\DBStorage;

$app = new Application();

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_sqlite',
        'path'     => __DIR__.'/../db.sqlite3',
    ),
));

$app['user.provider'] = function (Application $app) {
    return new DBUserProvider($app['db']);
};

$app['user.password.encoder'] = function () {
    return new BcryptEncoder();
};

$app['user.password_authenticator'] = function (Application $app) {
    return new PasswordAuthenticatedProvider($app['user.password.encoder']);
};

$app['user.token_generator'] = function (Application $app) {
    return new TokenGenerator($app['token_storage']);
};

$app['token_storage'] = function (Application $app) {
    return new DBStorage($app['db']);
};