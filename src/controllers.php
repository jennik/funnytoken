<?php

use \Silex\Application;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\JsonResponse;
use \FunnyToken\User;
use \Symfony\Component\Security\Core\Exception\AuthenticationException;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$app['debug'] = true;

$app->post('/token/new', function (Application $app, Request $request) {
    $userName = $request->request->get('user');
    $userPassword = $request->request->get('password');

    $user = $app['user.provider']->getUserByName($userName);

    if (!$app['user.password_authenticator']->isAuthenticated($user, $userPassword)) {
        throw new AuthenticationException('Wrong user credentials');
    }

    $token = $app['user.token_generator']->newToken($user);

    return new JsonResponse(['token' => $token], 201);
});

$app->get('/user/name', function (Application $app, Request $request) {
    $token = $request->query->get('token');

    $user = $app['user.provider']->getUserByToken($token);

    if (!$user) {
        throw new NotFoundHttpException("Not Found");
    }

    $name = $user->getName();

    return new JsonResponse(['name' => $name]);
});

$app->error(function (NotFoundHttpException $e, Request $request) {
    return new JsonResponse(['message' => $e->getMessage()], 404);
});

$app->error(function (AuthenticationException $e, Request $request) {
    return new JsonResponse(['message' => $e->getMessage()], 400);
});